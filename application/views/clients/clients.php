

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Clientes
            <small>Clientes</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Clientes</a></li>
            <li class="active">Clientes</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div id="messages"></div>

                <?php if($this->session->flashdata('success')): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php elseif($this->session->flashdata('error')): ?>
                    <div class="alert alert-error alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>




                <form id="clients" method="post" action="<?php echo base_url() ?>index.php/clients/registrar" >
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Datos Básicos</h3>
                            <small class="label bg-navy"><i class="fa fa-info"></i>  Solo los campos con Franja Azul son obligatorios</small>
                        </div>
                        <div class="box-body">
                            <div class="col-md-12  ">
                                <div class="form-group row">

                                    <div class="col-md-2">
                                        <label class="form-control-label" for="l0">Tipo Identificacion</label>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control requerido" name="tipoidentificacion" id="tipoidentificacion">
                                            <option  value="05">Cedula</option>
                                            <option  value="04">Ruc</option>
                                            <option  value="06">Pasaporte</option>
                                            <option  value="08">Identificacion del Exterior</option>
                                            <option  value="09">Placa</option>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <label class="form-control-label " for="l0">Identificacion</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input name="identificacion" type="text" class="form-control requerido validador_solonumero"
                                               placeholder="9999999999999" id="identificacion" value="">
                                    </div>




                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label class="form-control-label" for="l0">Nombres/Razón Social</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input name="nombrerazonsocial" type="text" class="form-control requerido mayuscula"
                                               placeholder="Razón Social" id="nombrerazonsocial" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label class="form-control-label" for="l0">Estado</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input name="estado" type="text" class="form-control requerido mayuscula"
                                               placeholder="Estado  M / F" id="estado" value="">
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label class="form-control-label" for="l0">Fecha de Creación</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input name="fechadecreacion" type="text" class="form-control requerido mayuscula"
                                               placeholder="Fecha de Creacion" id="fechadecreacion" value="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>



                    <button type="submit" class="btn btn-success "><i class="fa fa-save"  value="registrar" ></i> &nbsp;Guardar Cliente
                    </button>









                </form>
                <script>
                    $('#provincia').change(function () {
                        buscarciudad();
                    })

                    function buscarciudad() {
                        var provincia = $('#provincia').val();
                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/ciudades",
                            data: {
                                id_provincia: provincia,
                            },
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    $('#ciudad').empty();
                                    $(resp.datos).each(function (i, v) {
                                        $('#ciudad').append(' <option value=' + v.id + '>' + v.nombre + '</option>');
                                    });
                                }
                            }
                        })
                    }

                    $('#identificacion').keyup(function () {
                        if ($('#identificacion').val().length == 10) {
                            $('#tipoidentificacion').val("05");
                        } else if ($('#identificacion').val().length == 13) {
                            $('#tipoidentificacion').val("04");
                        }
                    });

                    $('#identificacion').change(function () {

                        var cedula = $(this).val();
                        var tipoidentificacion = $("#tipoidentificacion").val();

                        if (tipoidentificacion == "05") {
                            //Preguntamos si la cedula consta de 10 digitos
                            if (cedula.length == 10) {

                                //Obtenemos el digito de la region que sonlos dos primeros digitos
                                var digito_region = cedula.substring(0, 2);

                                //Pregunto si la region existe ecuador se divide en 24 regiones
                                if (digito_region >= 1 && digito_region <= 24) {

                                    // Extraigo el ultimo digito
                                    var ultimo_digito = cedula.substring(9, 10);

                                    //Agrupo todos los pares y los sumo
                                    var pares = parseInt(cedula.substring(1, 2)) + parseInt(cedula.substring(3, 4)) + parseInt(cedula.substring(5, 6)) + parseInt(cedula.substring(7, 8));

                                    //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
                                    var numero1 = cedula.substring(0, 1);
                                    var numero1 = (numero1 * 2);
                                    if (numero1 > 9) {
                                        var numero1 = (numero1 - 9);
                                    }

                                    var numero3 = cedula.substring(2, 3);
                                    var numero3 = (numero3 * 2);
                                    if (numero3 > 9) {
                                        var numero3 = (numero3 - 9);
                                    }

                                    var numero5 = cedula.substring(4, 5);
                                    var numero5 = (numero5 * 2);
                                    if (numero5 > 9) {
                                        var numero5 = (numero5 - 9);
                                    }

                                    var numero7 = cedula.substring(6, 7);
                                    var numero7 = (numero7 * 2);
                                    if (numero7 > 9) {
                                        var numero7 = (numero7 - 9);
                                    }

                                    var numero9 = cedula.substring(8, 9);
                                    var numero9 = (numero9 * 2);
                                    if (numero9 > 9) {
                                        var numero9 = (numero9 - 9);
                                    }

                                    var impares = numero1 + numero3 + numero5 + numero7 + numero9;

                                    //Suma total
                                    var suma_total = (pares + impares);

                                    //extraemos el primero digito
                                    var primer_digito_suma = String(suma_total).substring(0, 1);

                                    //Obtenemos la decena inmediata
                                    var decena = (parseInt(primer_digito_suma) + 1) * 10;

                                    //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
                                    var digito_validador = decena - suma_total;

                                    //Si el digito validador es = a 10 toma el valor de 0
                                    if (digito_validador == 10)
                                        var digito_validador = 0;

                                    //Validamos que el digito validador sea igual al de la cedula
                                    if (digito_validador == ultimo_digito) {
                                        aviso("ok", "Cedula Correcta", "", "");
                                        if (clientenoexiste(cedula) == true) {
                                            buscardatosensri(cedula);
                                        }

                                        // alert("la Cedula es correcta");
                                    } else {
                                        //  alert("la cedula es incorrecta");
                                    }

                                } else {
                                    // imprimimos en consola si la region no pertenece
                                    // console.log('Esta cedula no pertenece a ninguna region');
                                }
                            } else {
                                //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
                                //  alert("faltan digitos en la cedula");
                            }
                        } else if (tipoidentificacion == "04") {
                            if (cedula.length == 13) {
                                var number = cedula;
                                var dto = cedula.length;
                                var valor;
                                var acu = 0;

                                for (var i = 0; i < dto; i++) {
                                    valor = number.substring(i, i + 1);
                                    if (valor == 0 || valor == 1 || valor == 2 || valor == 3 || valor == 4 || valor == 5 || valor == 6 || valor == 7 || valor == 8 || valor == 9) {
                                        acu = acu + 1;
                                    }
                                }
                                if (acu == dto) {
                                    while (number.substring(10, 13) != 001) {
                                        alert('Los tres últimos dígitos no tienen el código del RUC 001.');
                                        return;
                                    }
                                    while (number.substring(0, 2) > 24) {
                                        alert('Los dos primeros dígitos no pueden ser mayores a 24.');
                                        return;
                                    }

                                    var porcion1 = number.substring(2, 3);
                                    if (porcion1 < 6) {
                                    }
                                    else {
                                        if (porcion1 == 6) {
                                        }
                                        else {
                                            if (porcion1 == 9) {
                                            }
                                        }
                                    }
                                }


                                clientenoexiste(cedula);

                                if (localStorage.getItem("verificacionexistencia")) {
                                    buscardatosensri(cedula);
                                }

                                //alert("Ruc correcto");
                            } else {
                                //   alert("faltan digitos en el ruc");
                            }
                        }

                    })

                    function clientenoexiste(dato) {
                        if (dato != "") {
                            var resultado;
                            var tipo = $("#tipoidentificacion").val();
                            $.ajax({
                                type: 'POST',
                                url: "https://azur.com.ec/plataforma/acciones/clientenoexiste",
                                data: {identificacion: dato, tipo: tipo, api_key2: "API_1_2_5a4492f2d5137"},
                                success: function (resp) {

                                    if (resp.respuesta == true) {
                                        localStorage.setItem("verificacionexistencia", true);
                                    } else if (resp.respuesta == "existe") {
                                        $.notify({
                                            title: "<strong>Error</strong> <br>",
                                            message: "<ul>" +
                                                "<li>El Cliente ya Existe</li>" +
                                                "<li>Si desea crear una sucursal lo puede hacer en la opcion Clientes</li>" +
                                                "</ul>",
                                        }, {
                                            type: 'error',
                                            mouse_over: 'pause'
                                        });
                                        localStorage.setItem("verificacionexistencia", false);
                                    } else {
                                        localStorage.setItem("verificacionexistencia", false);
                                    }
                                }
                            })
                        } else {
                            localStorage.setItem("verificacionexistencia", false);
                        }
                    }

                    function buscardatosensri(dato) {
                        if (dato != "") {
                            $.ajax({
                                type: 'POST',
                                url: "https://azur.com.ec/plataforma/acciones/datosdelgobierno",
                                data: {ruc: dato, api_key2: "API_1_2_5a4492f2d5137"},
                                success: function (resp) {

                                    if (resp.respuesta == true) {

                                        if ($("#nombrerazonsocial").val() == "") {
                                            $("#nombrerazonsocial").val(resp.datos.razon_social);
                                        }

                                    }
                                }
                            })
                        }
                    }

                </script>





    </section>
    <!-- /.content -->

</div>






<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!-- col-md-12 -->
</div>
<!-- /.row -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
