<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_clients');
        $this->load->model('Model_branch');
        $this->not_logged_in();

        $this->data['page_title'] = 'Registro Clientes';
        $this->load->helper(array('form','url'));
    }

    public function index()
    {

        $data['clientes']= $this->Model_clients->getClients();
        $data['user_permission'] = $this->permission;

        $this->render_template('clients/mostrar', $data);

    }

    public function  registrar()
    {
       $tipoidentificacion = $this->input->post("tipoidentificacion");
        $identificacion = $this->input->post("identificacion");
        $nombrerazonsocial = $this->input->post("nombrerazonsocial");
        $estado = $this->input->post("estado");
        $fechadecreacion = $this->input->post("fechadecreacion");


        $sucursal = $this->input->post("sucursal");
        $codigosucursal = $this->input->post("codigosucursal");
        $direccion = $this->input->post("direccion");
        $telefono = $this->input->post("telefono");
        $celular = $this->input->post("celular");
        $correo = $this->input->post("correo");
        $provincia = $this->input->post("provincia");
        $ciudad = $this->input->post("ciudad");
        $ezmatriz = $this->input->post("ezmatriz");

        $resultado = $this->Model_clients->registro($tipoidentificacion, $identificacion, $nombrerazonsocial, $estado, $fechadecreacion);

        $resultado = $this->Model_branch->registro( $sucursal, $codigosucursal, $direccion, $telefono, $celular, $correo, $provincia ,  $ciudad, $ezmatriz  );


        if(resultado){
            redirect("clients/correct");
        }else{
            $this->index();
        }



    }


    public  function correct(){

        $this->data['page_title'] = 'Clientes';

        if(!in_array('viewCategory', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

        $this->render_template('clients/mostrar', $this->data);


    }





}


