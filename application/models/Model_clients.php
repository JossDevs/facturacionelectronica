<?php

class Model_clients extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->database();

    }

    public function registro($identification_type, $identification, $names_companyname, $status, $creation_date)
    {
        return $this->db->insert("clients",[
                "identification_type"=>$identification_type ,
                "identification"=> $identification,
                "names_companyname"=>$names_companyname,
                "status"=> $status,"creation_date"=> $creation_date


            ]);



    }



    public function getClients()
    {
        $query=$this->db->get('clients');
        if($query->num_rows()>0)

            return $query;
        else
            return false;
    }








}



