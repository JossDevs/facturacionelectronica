<?php

class Model_branch extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->database();

    }

    public function registro($alias_branch, $branch_code, $direction ,$phone, $mobile, $mail ,$province, $city ,$eMatriz)
    {
        return $this->db->insert("branch_office",[
            "alias_branch"=>$alias_branch ,
            "branch_code"=> $branch_code,
            "direction"=>$direction,
            "phone"=>$phone,
            "mobile"=>$mobile,
            "mail"=>$mail,
            "province"=>$province,
            "city"=>$city,
            "eMatriz"=>$eMatriz

            ]);



    }


}

